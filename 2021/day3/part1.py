f = open('input')
lines = f.readlines()

ones = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

for line in lines:
    for i, bit in enumerate(line[:-1]):
        ones[i] += int(bit)
        
gamma = []
epsilon = []

for count in ones:
    if count > len(lines) / 2:
        gamma.append('1')
        epsilon.append('0')
    else:
        gamma.append('0')
        epsilon.append('1')
        


print(int("".join(gamma), 2) * int("".join(epsilon), 2))

