f = open('input')
lines = f.readlines()

oxygens = []
co2s = []

for line in lines:
    oxygens.append(line)
    co2s.append(line)
    
while len(oxygens) > 1:
    for i in range(12):
        ones = 0
        
        for line in oxygens:
            ones += int(line[i])

        common = '1' if ones >= len(oxygens) / 2 else '0'
            
        for line in oxygens.copy():
            if len(oxygens) > 1:
                if line[i] != common:
                    oxygens.remove(line)

while len(co2s) > 1:
    for i in range(12):
        ones = 0

        for line in co2s:
            ones += int(line[i])

        uncommon = '0' if ones >= len(co2s) / 2 else '1'

        for line in co2s.copy():
            if len(co2s) > 1:
                if line[i] != uncommon:
                    co2s.remove(line)
                    
print(int("".join(oxygens[0]), 2) * int("".join(co2s[0]), 2))

# 1004696 too low
