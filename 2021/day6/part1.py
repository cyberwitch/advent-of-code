f = open('input')
line = f.readline()

state = [int(fish) for fish in line.split(',')]

for _ in range(80):
    for i in range(len(state)):
        if state[i] == 0:
            state[i] = 6
            state.append(8)
        else:
            state[i] -= 1

print(len(state))
