f = open('input')
line = f.readline()

ages = [0] * 9

for fish in line.split(','):
    age = int(fish)
    ages[age] += 1

for _ in range(256):
    fish = ages.pop(0)
    ages.append(fish)
    ages[6] += fish

print(sum(ages))

# 1629 too low