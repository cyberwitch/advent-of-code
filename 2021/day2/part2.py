f = open('input')
lines = f.readlines()

horizontal = 0
depth = 0
aim = 0

for line in lines:
    command = line.split(' ')
    if command[0] == 'forward':
        horizontal += int(command[1])
        depth += aim * int(command[1])
    elif command[0] == 'down':
        aim += int(command[1])
    elif command[0] == 'up':
        aim -= int(command[1])

print(f'answer={horizontal * depth}')

