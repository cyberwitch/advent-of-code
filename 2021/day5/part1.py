import collections
import re

f = open('input')
lines = f.readlines()

xy_map = collections.defaultdict(int)

for line in lines:
    match = re.match(r'(\d+),(\d+) -> (\d+),(\d+)', line)
    x1, y1, x2, y2 = match.groups()
    x1, y1, x2, y2 = int(x1), int(y1), int(x2), int(y2)

    if x1 == x2:
        for y in range(min(y1, y2), max(y1, y2) + 1):
            xy_map[(x1, y)] += 1
    elif y1 == y2:
        for x in range(min(x1, x2), max(x1, x2) + 1):
            xy_map[(x, y1)] += 1

print(len([0 for value in xy_map.values() if value > 1]))
