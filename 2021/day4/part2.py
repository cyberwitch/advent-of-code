def print_answer(b, num):
    total = 0
    for n in b:
        if n is not None:
            total += int(n)

    print(total * int(num))


f = open('input')

numbers = f.readline().strip().split(',')

boards = []

for line in f.readlines():
    if line == '\n':
        boards.append([])
    else:
        boards[-1] = boards[-1] + line.split()

won = []

for number in numbers:
    for board_num, board in enumerate(boards):
        if board_num not in won:
            for i in range(25):
                if board[i] == number:
                    board[i] = None

            winner = False
            for i in range(5):
                winner = True
                for j in range(5):
                    if board[i * 5 + j] is not None:
                        winner = False
                if winner is True:
                    won.append(board_num)
                    if len(won) == len(boards):
                        print_answer(board, number)
                    else:
                        break

            if winner is False:
                for i in range(5):
                    winner = True
                    for j in range(5):
                        if board[j * 5 + i] is not None:
                            winner = False
                    if winner is True:
                        won.append(board_num)
                        if len(won) == len(boards):
                            print_answer(board, number)
                        else:
                            break

# 7164 too low