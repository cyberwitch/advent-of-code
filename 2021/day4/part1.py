def print_answer(b, num):
    total = 0
    for n in b:
        if n is not None:
            total += int(n)

    print(total * int(num))


f = open('input')

numbers = f.readline().strip().split(',')

boards = []

for line in f.readlines():
    if line == '\n':
        boards.append([])
    else:
        boards[-1] = boards[-1] + line.split()

for number in numbers:
    for board in boards:
        for i in range(25):
            if board[i] == number:
                board[i] = None

        for i in range(5):
            winner = True
            for j in range(5):
                if board[i * 5 + j] is not None:
                    winner = False
            if winner is True:
                print_answer(board, number)
                exit()

        for i in range(5):
            winner = True
            for j in range(5):
                if board[j * 5 + i] is not None:
                    winner = False
            if winner is True:
                print_answer(board, number)
                exit()
