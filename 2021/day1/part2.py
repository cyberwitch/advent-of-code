f = open('input')
lines = f.readlines()

arr = []
larger = 0
for line in lines:
    this = int(line)
    if len(arr) == 3:
        if sum(arr) < arr[1] + arr[2] + this:
            larger += 1
    arr.append(this)
    if len(arr) == 4:
        arr.pop(0)

print(larger)
