f = open('input')
lines = f.readlines()

last = 0
larger = 0
for line in lines:
    this = int(line)
    if last and this > last:
        larger += 1
    last = this

print(larger)
