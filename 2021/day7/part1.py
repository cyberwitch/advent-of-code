f = open('input')
positions = f.readline().split(',')
positions = [int(p) for p in positions]

best_fuel = None
best_position = None
for p in range(min(positions), max(positions) + 1):
    fuel = 0
    for crab in positions:
        fuel += abs(crab - p)

    if best_fuel is None or best_fuel > fuel:
        best_fuel = fuel
        best_position = p

print(best_fuel)
