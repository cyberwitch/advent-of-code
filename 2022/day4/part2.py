f = open('input')
lines = f.readlines()

overlaps = 0

for line in lines:
    ranges = [r.split('-') for r in line.strip('\n').split(',')]
    start1 = int(ranges[0][0])
    end1 = int(ranges[0][1])
    start2 = int(ranges[1][0])
    end2 = int(ranges[1][1])

    if (start1 <= end2 and end1 >= start2) or (start2 <= end1 and end2 >= start1):
        overlaps += 1

print(overlaps)
