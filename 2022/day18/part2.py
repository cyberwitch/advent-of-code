from collections import deque

LOWER_BOUNDARY = 0
UPPER_BOUNDARY = 20

xyz = dict()

f = open('input')

for line in f.readlines():
    x, y, z = (int(n) for n in line.rstrip().split(','))
    xyz[x, y, z] = '#'


checks = deque()
checks.append((LOWER_BOUNDARY, LOWER_BOUNDARY, LOWER_BOUNDARY))

sides = 0


def visit(_x, _y, _z, _sides):
    if not(LOWER_BOUNDARY <= _x <= UPPER_BOUNDARY and LOWER_BOUNDARY <= _y <= UPPER_BOUNDARY and LOWER_BOUNDARY <= _z <= UPPER_BOUNDARY) or xyz.get((_x, _y, _z)) == '.':
        return _sides

    if xyz.get((_x, _y, _z)) == '#':
        return _sides

    xyz[_x, _y, _z] = '.'

    if xyz.get((_x + 1, _y, _z)) == '#':
        _sides += 1
    else:
        checks.append((_x + 1, _y, _z))

    if xyz.get((_x - 1, _y, _z)) == '#':
        _sides += 1
    else:
        checks.append((_x - 1, _y, _z))

    if xyz.get((_x, _y + 1, _z)) == '#':
        _sides += 1
    else:
        checks.append((_x, _y + 1, _z))

    if xyz.get((_x, _y - 1, _z)) == '#':
        _sides += 1
    else:
        checks.append((_x, _y - 1, _z))

    if xyz.get((_x, _y, _z + 1)) == '#':
        _sides += 1
    else:
        checks.append((_x, _y, _z + 1))

    if xyz.get((_x, _y, _z - 1)) == '#':
        _sides += 1
    else:
        checks.append((_x, _y, _y - 1))

    return _sides


while len(checks):
    x, y, z = checks.pop()

    sides = visit(x, y, z, sides)

print(sides)

# 3225 too high
# 3217 too high