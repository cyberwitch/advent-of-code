from collections import defaultdict


def get_defaultdict_set():
    return defaultdict(set)


xyz = defaultdict(get_defaultdict_set)
yzx = defaultdict(get_defaultdict_set)
zxy = defaultdict(get_defaultdict_set)

f = open('input')

for line in f.readlines():
    x, y, z = (int(n) for n in line.rstrip().split(','))
    xyz[x][y].add(z)
    yzx[y][z].add(x)
    zxy[z][x].add(y)

sides = 0

for x in xyz:
    for y in xyz[x]:
        for z in xyz[x][y]:
            if z - 1 not in xyz[x][y]:
                sides += 1
            if z + 1 not in xyz[x][y]:
                sides += 1

for y in yzx:
    for z in yzx[y]:
        for x in yzx[y][z]:
            if x - 1 not in yzx[y][z]:
                sides += 1
            if x + 1 not in yzx[y][z]:
                sides += 1

for z in zxy:
    for x in zxy[z]:
        for y in zxy[z][x]:
            if y - 1 not in zxy[z][x]:
                sides += 1
            if y + 1 not in zxy[z][x]:
                sides += 1

print(sides)
