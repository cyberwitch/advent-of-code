f = open('input')
lines = f.readlines()

grid = []
scores = []

for line in lines:
    grid.append(line.rstrip())
    scores.append([0] * (len(line) - 1))

for y in range(len(grid)):
    for x in range(len(grid[0])):
        score = 0
        tallest = grid[y][x]
        for next_tree in range(y - 1, -1, -1):
            if grid[next_tree][x] < grid[y][x]:
                score += 1
                tallest = grid[next_tree][x]
            else:
                score += 1
                break

        scores[y][x] = score

        score = 0
        tallest = grid[y][x]
        for next_tree in range(y + 1, len(grid)):
            if grid[next_tree][x] < grid[y][x]:
                score += 1
                tallest = grid[next_tree][x]
            else:
                score += 1
                break

        scores[y][x] *= score

        score = 0
        tallest = grid[y][x]
        for next_tree in range(x - 1, -1, -1):
            if grid[y][next_tree] < grid[y][x]:
                score += 1
                tallest = grid[x][next_tree]
            else:
                score += 1
                break

        scores[y][x] *= score

        score = 0
        tallest = grid[y][x]
        for next_tree in range(x + 1, len(grid[0])):
            if grid[y][next_tree] < grid[y][x]:
                score += 1
                tallest = grid[y][next_tree]
            else:
                score += 1
                break

        scores[y][x] *= score

highest = 0

for y in range(len(scores)):
    for x in range(len(scores[0])):
        if scores[y][x] > highest:
            highest = scores[y][x]

print(highest)