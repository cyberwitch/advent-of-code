f = open('input')
lines = f.readlines()

grid = []

for line in lines:
    grid.append(line.rstrip())

visible = set()

for x in range(len(grid[0])):
    visible.add((x, 0))
    visible.add((x, len(grid) - 1))

    tallest = grid[0][x]
    for y in range(1, len(grid) - 1):
        if grid[y][x] > tallest:
            visible.add((x, y))
            tallest = grid[y][x]

    tallest = grid[len(grid) - 1][x]
    for y in range(len(grid) - 2, 1, -1):
        if grid[y][x] > tallest:
            visible.add((x, y))
            tallest = grid[y][x]

for y in range(len(grid)):
    visible.add((0, y))
    visible.add((len(grid[0]) - 1, y))

    tallest = grid[y][0]
    for x in range(1, len(grid[0]) - 1):
        if grid[y][x] > tallest:
            visible.add((x, y))
            tallest = grid[y][x]

    tallest = grid[y][len(grid[0]) - 1]
    for x in range(len(grid[0]) - 2, 0, -1):
        if grid[y][x] > tallest:
            visible.add((x, y))
            tallest = grid[y][x]

print(sorted(visible))
print(len(visible))
