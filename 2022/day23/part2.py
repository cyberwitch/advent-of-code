elves = []
proposals = []


def elves_include_any(_directions):
    for direction in _directions:
        if direction in elves:
            return True

    return False


def proposals_include_duplicates(direction):
    count = 0

    for proposal in proposals:
        if proposal == direction:
            count += 1

    return count > 1


f = open('input')

for y, line in enumerate(f.readlines()):
    for x, character in enumerate(line):
        if character == '#':
            elves.append((x, y))

_round = 0

while proposals != [None] * len(elves):
    proposals = []

    for i, elf in enumerate(elves):
        N = (elf[0], elf[1] - 1)
        NE = (elf[0] + 1, elf[1] - 1)
        NW = (elf[0] - 1, elf[1] - 1)
        S = (elf[0], elf[1] + 1)
        SE = (elf[0] + 1, elf[1] + 1)
        SW = (elf[0] - 1, elf[1] + 1)
        W = (elf[0] - 1, elf[1])
        E = (elf[0] + 1, elf[1])

        directions = [N, NE, NW, S, SE, SW, W, NW, SW, E, NE, SE]

        for j in range(_round % 4):
            directions.append(directions.pop(0))
            directions.append(directions.pop(0))
            directions.append(directions.pop(0))

        if not elves_include_any(directions):
            proposals.append(None)
        elif not elves_include_any(directions[:3]):
            proposals.append(directions[0])
        elif not elves_include_any(directions[3:6]):
            proposals.append(directions[3])
        elif not elves_include_any(directions[6:9]):
            proposals.append(directions[6])
        elif not elves_include_any(directions[9:]):
            proposals.append(directions[9])
        else:
            proposals.append(None)

    for i, elf in enumerate(elves):
        if proposals[i] is None:
            pass
        elif not proposals_include_duplicates(proposals[i]):
            elves[i] = proposals[i]

    _round += 1

print(_round)
