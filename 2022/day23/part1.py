elves = []
proposals = []


def elves_include_any(_directions):
    for direction in _directions:
        if direction in elves:
            return True

    return False


def proposals_include_duplicates(direction):
    count = 0

    for proposal in proposals:
        if proposal == direction:
            count += 1

    return count > 1


f = open('input')

for y, line in enumerate(f.readlines()):
    for x, character in enumerate(line):
        if character == '#':
            elves.append((x, y))

for _round in range(10):
    proposals = []

    for i, elf in enumerate(elves):
        N = (elf[0], elf[1] - 1)
        NE = (elf[0] + 1, elf[1] - 1)
        NW = (elf[0] - 1, elf[1] - 1)
        S = (elf[0], elf[1] + 1)
        SE = (elf[0] + 1, elf[1] + 1)
        SW = (elf[0] - 1, elf[1] + 1)
        W = (elf[0] - 1, elf[1])
        E = (elf[0] + 1, elf[1])

        directions = [N, NE, NW, S, SE, SW, W, NW, SW, E, NE, SE]

        for j in range(_round % 4):
            directions.append(directions.pop(0))
            directions.append(directions.pop(0))
            directions.append(directions.pop(0))

        if not elves_include_any(directions):
            proposals.append(elf)
        elif not elves_include_any(directions[:3]):
            proposals.append(directions[0])
        elif not elves_include_any(directions[3:6]):
            proposals.append(directions[3])
        elif not elves_include_any(directions[6:9]):
            proposals.append(directions[6])
        elif not elves_include_any(directions[9:]):
            proposals.append(directions[9])
        else:
            proposals.append(elf)

    for i, elf in enumerate(elves):
        if proposals[i] == elf:
            pass
        elif not proposals_include_duplicates(proposals[i]):
            elves[i] = proposals[i]

min_x = None
max_x = None
min_y = None
max_y = None

for elf in elves:
    if min_x is None or elf[0] < min_x:
        min_x = elf[0]
    if max_x is None or elf[0] > max_x:
        max_x = elf[0]
    if min_y is None or elf[1] < min_y:
        min_y = elf[1]
    if max_y is None or elf[1] > max_y:
        max_y = elf[1]

print((max_x - min_x + 1) * (max_y - min_y + 1) - len(elves))
