import re
from dataclasses import dataclass


@dataclass
class Monkey:
    items: list[int]
    operator: str
    operand: str
    divisible_by: int
    if_true: int
    if_false: int

    inspections = 0

    def toss_to(self, item):
        self.items.append(item)

    def do_turn(self) -> list[(int, int)]:
        """
        Returns a list of items and which monkey to toss them to
        """

        _tosses = []

        for item in self.items:
            self.inspections += 1

            _operand = item if self.operand == 'old' else int(self.operand)

            if self.operator == '+':
                item += _operand
            else:
                item *= _operand

            item //= 3

            if item % self.divisible_by:
                _tosses.append((item, self.if_false))
            else:
                _tosses.append((item, self.if_true))

        self.items = []

        return _tosses


monkeys = []

f = open('input')
lines = f.readlines()

i = 0
while i < len(lines):
    kwargs = {}

    items = lines[i + 1].replace(',', '').split()
    kwargs['items'] = [int(item) for item in items[2:]]

    pattern = re.compile(r'.*([*+])\s+(\d+|old)')
    result = pattern.match(lines[i + 2])
    kwargs['operator'] = result.groups()[0]
    kwargs['operand'] = result.groups()[1]

    kwargs['divisible_by'] = int(lines[i + 3].split()[-1])
    kwargs['if_true'] = int(lines[i + 4].split()[-1])
    kwargs['if_false'] = int(lines[i + 5].split()[-1])

    i += 7

    monkeys.append(Monkey(**kwargs))

for i in range(20):
    for monkey in monkeys:
        tosses = monkey.do_turn()

        for toss in tosses:
            monkeys[toss[1]].toss_to(toss[0])

top_two = sorted(monkeys, key=lambda m: m.inspections, reverse=True)[:2]

print(top_two[0].inspections * top_two[1].inspections)
