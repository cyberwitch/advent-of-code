f = open('input')
lines = f.readlines()

outcomes = {
    'A': {
        'X': 3,
        'Y': 6,
        'Z': 0,
    },
    'B': {
        'X': 0,
        'Y': 3,
        'Z': 6,
    },
    'C': {
        'X': 6,
        'Y': 0,
        'Z': 3,
    },
}

choice_scores = {
    'X': 1,
    'Y': 2,
    'Z': 3,
}

score = 0

for line in lines:
    choices = line.split(' ')
    them = choices[0]
    me = choices[1].strip('\n')
    score += outcomes[them][me] + choice_scores[me]

print(score)
