f = open('input')
lines = f.readlines()

ROCK = 'A'
PAPER = 'B'
SCISSORS = 'C'

LOSE = 'X'
DRAW = 'Y'
WIN = 'Z'

choices = {
    ROCK: {
        LOSE: SCISSORS,
        DRAW: ROCK,
        WIN: PAPER,
    },
    PAPER: {
        LOSE: ROCK,
        DRAW: PAPER,
        WIN: SCISSORS,
    },
    SCISSORS: {
        LOSE: PAPER,
        DRAW: SCISSORS,
        WIN: ROCK,
    },
}

choice_scores = {
    ROCK: 1,
    PAPER: 2,
    SCISSORS: 3,
}

outcome_values = {
    LOSE: 0,
    DRAW: 3,
    WIN: 6,
}

score = 0

for line in lines:
    columns = line.split(' ')
    them = columns[0]
    outcome = columns[1].strip('\n')
    me = choices[them][outcome]
    score += outcome_values[outcome] + choice_scores[me]

print(score)
