class Registers:
    x = 1


class Instruction:
    cycles = 1

    def execute(self, registers):
        pass


class Noop(Instruction):
    pass


class AddX(Instruction):
    cycles = 2

    def __init__(self, increment):
        self.increment = increment

    def execute(self, registers):
        registers.x += self.increment


class CPU:
    cycle = 0
    registers = Registers()
    sum_signal_strengths = 0

    def execute(self, instruction: Instruction):
        for cycle in range(instruction.cycles):
            self.cycle += 1

            if (self.cycle - 20) % 40 == 0:
                self.sum_signal_strengths += self.cycle * self.registers.x

        instruction.execute(self.registers)


cpu = CPU()

f = open('input')

for line in f.readlines():
    if line.startswith('noop'):
        cpu.execute(Noop())
    elif line.startswith('addx'):
        cpu.execute(AddX(int(line.rsplit()[1])))

print(cpu.sum_signal_strengths)
