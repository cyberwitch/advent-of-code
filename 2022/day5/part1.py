import re

f = open('input')
lines = f.readlines()

stack_count = len(lines[0]) // 4
stacks = []

for i in range(stack_count):
    stacks.append([])

i = 0

for i, line in enumerate(lines):
    if line.startswith(' 1'):
        break

    for j, stack in enumerate(stacks):
        if line[j * 4 + 1] != ' ':
            stack.append(line[j * 4 + 1])

for line in lines[i + 2:]:
    match = re.match(r'move (\d+) from (\d+) to (\d+)', line)

    if match:
        groups = match.groups()
        for j in range(int(groups[0])):
            stacks[int(groups[2]) - 1].insert(0, stacks[int(groups[1]) - 1].pop(0))

print(''.join(stack[0] for stack in stacks))
