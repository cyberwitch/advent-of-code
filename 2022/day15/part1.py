import re

ROW = 2000000

pattern = re.compile(r'Sensor at x=(-?\d+), y=(-?\d+): closest beacon is at x=(-?\d+), y=(-?\d+)')

f = open('input')

beacons = set()

non_beacons = set()

for line in f.readlines():
    match = pattern.match(line)

    sx, sy, bx, by = [int(s) for s in match.groups()]

    if by == ROW:
        beacons.add(bx)

    distance = abs(sx - bx) + abs(sy - by)
    vertical_distance = abs(sy - ROW)
    horizontal_distance = max(0, distance - vertical_distance)

    for x in range(sx - horizontal_distance, sx + horizontal_distance + 1):
        non_beacons.add(x)

print(len(non_beacons - beacons))
