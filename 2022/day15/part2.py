import re
from collections import defaultdict

BOUNDARY = 4000000


class Cave:
    non_beacons = defaultdict(set)

    def add_non_beacons(self, x1, x2, _y):
        if not 0 <= _y <= BOUNDARY:
            return

        x1 = max(0, x1)
        x2 = min(BOUNDARY, x2)

        for _range in self.non_beacons[_y]:
            if _range[1] >= x1 - 1 and _range[0] <= x2 + 1:
                x1 = min(_range[0], x1)
                x2 = max(_range[1], x2)

        self.non_beacons[_y] = {_range for _range in self.non_beacons[_y] if _range[1] < x1 - 1 or _range[0] > x2 + 1}

        self.non_beacons[_y].add((x1, x2))

    def get_tuning_frequency(self):
        for row in self.non_beacons:
            if self.non_beacons[row] != {(0, BOUNDARY)}:
                if len(self.non_beacons[row]) == 1:
                    if self.non_beacons[row][0][0] == 1:
                        return row
                    else:
                        return BOUNDARY * 4000000 + row
                else:
                    for _range in self.non_beacons[row]:
                        if _range[0] == 0:
                            return (_range[1] + 1) * 4000000 + row
                        else:
                            return (_range[0] - 1) * 4000000 + row


pattern = re.compile(r'Sensor at x=(-?\d+), y=(-?\d+): closest beacon is at x=(-?\d+), y=(-?\d+)')

f = open('input')

cave = Cave()

for line in f.readlines():
    match = pattern.match(line)

    sx, sy, bx, by = [int(s) for s in match.groups()]

    distance = abs(sx - bx) + abs(sy - by)

    for y in range(sy - distance, sy + distance + 1):
        vertical_distance = abs(sy - y)

        if vertical_distance <= distance:
            horizontal_distance = max(0, distance - vertical_distance)
            cave.add_non_beacons(sx - horizontal_distance, sx + horizontal_distance, y)

print(cave.get_tuning_frequency())
