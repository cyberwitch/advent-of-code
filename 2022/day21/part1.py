monkeys = {}

f = open('input')

for line in f.readlines():
    parts = line.rstrip().split()
    monkey = parts[0].rstrip(':')

    if len(parts) == 2:
        monkeys[monkey] = int(parts[1])
    else:
        monkeys[monkey] = parts[1:]


def get_monkey(m) -> int:
    if not isinstance(monkeys[m], int):
        if monkeys[m][1] == '+':
            monkeys[m] = get_monkey(monkeys[m][0]) + get_monkey(monkeys[m][2])
        elif monkeys[m][1] == '-':
            monkeys[m] = get_monkey(monkeys[m][0]) - get_monkey(monkeys[m][2])
        elif monkeys[m][1] == '*':
            monkeys[m] = get_monkey(monkeys[m][0]) * get_monkey(monkeys[m][2])
        elif monkeys[m][1] == '/':
            monkeys[m] = get_monkey(monkeys[m][0]) // get_monkey(monkeys[m][2])

    return monkeys[m]


print(get_monkey('root'))
