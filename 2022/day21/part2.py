from typing import Union

monkeys = {}

f = open('input')

for line in f.readlines():
    parts = line.rstrip().split()
    monkey = parts[0].rstrip(':')

    if monkey == 'root':
        parts[2] = '=='

    if len(parts) == 2:
        monkeys[monkey] = int(parts[1])
    else:
        monkeys[monkey] = parts[1:]


def get_monkey(m) -> Union[int, bool]:
    if not isinstance(monkeys[m], int):
        if monkeys[m][1] == '+':
            return get_monkey(monkeys[m][0]) + get_monkey(monkeys[m][2])
        elif monkeys[m][1] == '-':
            return get_monkey(monkeys[m][0]) - get_monkey(monkeys[m][2])
        elif monkeys[m][1] == '*':
            return get_monkey(monkeys[m][0]) * get_monkey(monkeys[m][2])
        elif monkeys[m][1] == '/':
            return get_monkey(monkeys[m][0]) // get_monkey(monkeys[m][2])
        elif monkeys[m][1] == '==':
            return get_monkey(monkeys[m][0]) == get_monkey(monkeys[m][2])
    else:
        return monkeys[m]


for humn in range(10000000):
    if humn % 10000 == 0:
        print(f'{humn=}')

    monkeys['humn'] = humn
    if get_monkey('root') is True:
        print(humn)
        exit()
