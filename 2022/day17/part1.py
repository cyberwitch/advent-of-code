from dataclasses import dataclass


class Rock:
    shape = []

    @property
    def height(self):
        return len(self.shape)

    @property
    def width(self):
        max_width = 0

        for row in self.shape:
            max_width = max(max_width, len(row))

        return max_width


class Rock1(Rock):
    shape = ['####']


class Rock2(Rock):
    shape = ['.#.', '###', '.#.']


class Rock3(Rock):
    shape = ['..#', '..#', '###']


class Rock4(Rock):
    shape = ['#', '#', '#', '#']


class Rock5(Rock):
    shape = ['##', '##']


def rock_generator():
    while True:
        yield Rock1()
        yield Rock2()
        yield Rock3()
        yield Rock4()
        yield Rock5()


@dataclass
class Directions:
    directions: str
    direction_pointer = 0

    @property
    def next_direction(self):
        direction = self.directions[self.direction_pointer]
        self.direction_pointer += 1
        if self.direction_pointer >= len(self.directions):
            self.direction_pointer = 0
        return direction


class Chamber:
    generator = rock_generator()
    boundaries = {(0, -1), (1, -1), (2, -1), (3, -1), (4, -1), (5, -1), (6, -1)}
    height = -1

    def __init__(self, directions):
        self.directions = Directions(directions)

    def shift_rock(self, rock, x, y):
        if self.directions.next_direction == '<':
            for _y, row in enumerate(rock.shape):
                for _x, value in enumerate(row):
                    if value == '#' and (x + _x - 1, y - _y) in self.boundaries:
                        return x
            return max(0, x - 1)
        else:
            for _y, row in enumerate(rock.shape):
                for _x, value in enumerate(row):
                    if value == '#' and (x + _x + 1, y - _y) in self.boundaries:
                        return x
            return min(7 - rock.width, x + 1)

    def lower_rock(self, rock, x, y):
        for _y, row in enumerate(rock.shape):
            for _x, value in enumerate(row):
                if value == '#' and (x + _x, y - _y - 1) in self.boundaries:
                    return y

        return max(0, y - 1)

    def add_boundaries(self, rock, x, y):
        for _y, row in enumerate(rock.shape):
            for _x, value in enumerate(row):
                if value == '#':
                    self.boundaries.add((x + _x, y - _y))

    def drop_rocks(self):
        rock = next(self.generator)
        x = 2
        y = self.height + 3 + rock.height

        old_y = None
        while old_y != y:
            old_y = y
            x = self.shift_rock(rock, x, y)
            y = self.lower_rock(rock, x, y)

        self.add_boundaries(rock, x, y)

        self.height = max(bound[1] for bound in self.boundaries)


f = open('input')

chamber = Chamber(f.readline().rstrip())

for i in range(2022):
    chamber.drop_rocks()

print(chamber.height + 1)
