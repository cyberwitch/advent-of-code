from itertools import product

DIGITS = '=-012'


def snafu_to_decimal(snafu: str):
    total = 0

    for i, digit in enumerate(snafu):
        multiplier = pow(5, len(snafu) - i - 1)
        total += multiplier * (DIGITS.index(digit) - 2)

    return total


def decimal_to_snafu(number: int):
    def decimal_to_base_five(n):
        while n > 0:
            n, d = divmod(n, 5)
            yield d

    base_five = list(decimal_to_base_five(number))

    carry = 0
    for i in range(len(base_five)):
        digit = base_five[i]
        digit += carry
        carry = 0

        while digit > 2:
            carry += 1
            digit -= 5

        base_five[i] = digit

    return ''.join(DIGITS[i + 2] for i in reversed(base_five))


decimal = 0

f = open('input')
for line in f.readlines():
    decimal += snafu_to_decimal(line.rstrip())

print(decimal_to_snafu(decimal))
