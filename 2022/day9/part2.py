f = open('input')
lines = f.readlines()


class Point:
    x = 0
    y = 0

    def save(self):
        return self.x, self.y

    def update(self, head):
        diff_x = head.x - self.x
        diff_y = head.y - self.y

        if not diff_x and abs(diff_y) > 1:
            if diff_y > 1:
                self.y += 1
            else:
                self.y -= 1
        elif not diff_y and abs(diff_x) > 1:
            if diff_x > 1:
                self.x += 1
            else:
                self.x -= 1
        elif abs(diff_x) > 1 and abs(diff_y) > 1:
            if diff_x > 1:
                self.x += 1
            else:
                self.x -= 1

            if diff_y > 1:
                self.y += 1
            else:
                self.y -= 1
        elif diff_x > 1:
            self.x += 1
            self.y += diff_y
        elif diff_x < -1:
            self.x -= 1
            self.y += diff_y
        elif diff_y > 1:
            self.y += 1
            self.x += diff_x
        elif diff_y < -1:
            self.y -= 1
            self.x += diff_x

    def __repr__(self):
        return f'({self.x}, {self.y})'

    def __str__(self):
        return f'({self.x}, {self.y})'


knots = [Point() for i in range(10)]

tail_visited = set()

for line in lines:
    direction, distance = line.split()

    for i in range(int(distance)):
        if direction == 'R':
            knots[0].x += 1
        elif direction == 'L':
            knots[0].x -= 1
        elif direction == 'U':
            knots[0].y += 1
        else:
            knots[0].y -= 1

        for j in range(1, len(knots)):
            knots[j].update(knots[j - 1])

        tail_visited.add(knots[9].save())

print(len(tail_visited))
