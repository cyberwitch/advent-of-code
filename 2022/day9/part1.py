f = open('input')
lines = f.readlines()


class Point:
    x = 0
    y = 0

    def save(self):
        return self.x, self.y

    def __str__(self):
        return f'({self.x}, {self.y})'


head = Point()
tail = Point()

tail_visited = set()

for line in lines:
    direction, distance = line.split()

    for i in range(int(distance)):
        if direction == 'R':
            head.x += 1
        elif direction == 'L':
            head.x -= 1
        elif direction == 'U':
            head.y += 1
        else:
            head.y -= 1

        diff_x = head.x - tail.x
        diff_y = head.y - tail.y

        if not diff_x and abs(diff_y) > 1:
            if diff_y > 1:
                tail.y += 1
            else:
                tail.y -= 1
        elif not diff_y and abs(diff_x) > 1:
            if diff_x > 1:
                tail.x += 1
            else:
                tail.x -= 1
        elif diff_x > 1:
            tail.x += 1
            tail.y += diff_y
        elif diff_x < -1:
            tail.x -= 1
            tail.y += diff_y
        elif diff_y > 1:
            tail.y += 1
            tail.x += diff_x
        elif diff_y < -1:
            tail.y -= 1
            tail.x += diff_x

        tail_visited.add(tail.save())

print(len(tail_visited))
