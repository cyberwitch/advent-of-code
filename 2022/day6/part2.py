f = open('input')
line = f.readline()

for i in range(13, len(line)):
    if len(set(*line[i-14:i].split())) == 14:
        print(i)
        break
