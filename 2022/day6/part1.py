f = open('input')
line = f.readline()

for i in range(3, len(line)):
    if len(set(*line[i-4:i].split())) == 4:
        print(i)
        break
