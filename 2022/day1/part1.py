f = open('input')
lines = f.readlines()

elves = [0]

for line in lines:
    if line == '\n':
        elves.append(0)
    else:
        elves[-1] += int(line)

print(max(elves))
