f = open('input')
lines = f.readlines()

elves = [0]

for line in lines:
    if line == '\n':
        elves.append(0)
    else:
        elves[-1] += int(line)

first = max(elves)
elves.remove(first)
second = max(elves)
elves.remove(second)
third = max(elves)

print(first + second + third)
