import json


def compare(a, b):
    if isinstance(a, int) and isinstance(b, int):
        if a < b:
            return True
        elif a > b:
            return False
    elif isinstance(a, list) and isinstance(b, list):
        j = 0

        for j in range(len(a)):
            if j >= len(b):
                return False

            is_correct_order = compare(a[j], b[j])

            if is_correct_order is True:
                return True
            elif is_correct_order is False:
                return False

        if len(b) > len(a):
            return True
    elif isinstance(a, int):
        return compare([a], b)
    else:
        return compare(a, [b])


f = open('input')
lines = f.readlines()

indices = 0

i = 0
while i < len(lines):
    left = json.loads(lines[i])
    right = json.loads(lines[i + 1])

    if compare(left, right):
        indices += i // 3 + 1

    i += 3

print(indices)
