import json
from functools import cmp_to_key


def compare(a, b):
    if isinstance(a, int) and isinstance(b, int):
        if a < b:
            return 1
        elif a > b:
            return -1
    elif isinstance(a, list) and isinstance(b, list):
        j = 0

        for j in range(len(a)):
            if j >= len(b):
                return -1

            is_correct_order = compare(a[j], b[j])

            if is_correct_order == 1:
                return 1
            elif is_correct_order == -1:
                return -1

        if len(b) > len(a):
            return 1
    elif isinstance(a, int):
        return compare([a], b)
    else:
        return compare(a, [b])

    return 0


f = open('input')
lines = f.readlines()

packets = [[[2]], [[6]]]

for line in lines:
    if line != '\n':
        packets.append(json.loads(line))

sorted_packets = sorted(packets, key=cmp_to_key(compare), reverse=True)

print((sorted_packets.index([[2]]) + 1) * (sorted_packets.index([[6]]) + 1))
