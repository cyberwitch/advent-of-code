from dataclasses import dataclass


@dataclass
class Point:
    x: int
    y: int

    def save(self):
        return self.x, self.y

    def __str__(self):
        return f'({self.x}, {self.y})'


class Cave:
    cave = {}
    sand = Point(500, 0)
    lowest_line = 0

    @property
    def sand_count(self):
        return len([value for value in self.cave.values() if value == 'o'])

    def add_line(self, a: Point, b: Point):
        if a.x == b.x:
            step = 1 if a.y < b.y else -1
            for y in range(a.y, b.y + step, step):
                self.cave[(a.x, y)] = '#'
        else:
            step = 1 if a.x < b.x else -1
            for x in range(a.x, b.x + step, step):
                self.cave[(x, a.y)] = '#'

        if max(a.y, b.y) > self.lowest_line:
            self.lowest_line = max(a.y, b.y)

    def move_sand(self):
        self.sand.y += 1

        if self.sand.save() in self.cave:
            self.sand.x -= 1

            if self.sand.save() in self.cave:
                self.sand.x += 2

                if self.sand.save() in self.cave:
                    self.sand.x -= 1

        if self.sand.save() in self.cave or self.sand.y == self.lowest_line + 2:
            self.sand.y -= 1
            self.cave[self.sand.save()] = 'o'

            if self.sand.x == 500 and self.sand.y == 0:
                return False

            self.sand = Point(500, 0)

        return True


f = open('input')
lines = f.readlines()

cave = Cave()

for line in lines:
    points = line.rstrip().split(' -> ')

    for i in range(len(points) - 1):
        x1, y1 = [int(x) for x in points[i].split(',')]
        x2, y2 = [int(y) for y in points[i + 1].split(',')]

        cave.add_line(Point(x1, y1), Point(x2, y2))


while cave.move_sand():
    pass

print(cave.sand_count)