class Directory:
    def __init__(self, parent):
        self.children = {}
        self.files = {}
        self.parent = parent

    @property
    def size(self):
        if not self.children and not self.files:
            return 0

        size = sum(self.files.values())
        size += sum(child.size for child in self.children.values())
        return size

    def cd(self, name):
        if name not in self.children:
            self.children[name] = Directory(self)

        return self.children[name]

    def mkdir(self, name):
        if name not in self.children:
            self.children[name] = Directory(self)

    def touch(self, size, name):
        self.files[name] = int(size)


def sum_total_sizes(directory):
    sum_total_size = 0

    if directory.size <= 100000:
        sum_total_size += directory.size

    for child in directory.children.values():
        sum_total_size += sum_total_sizes(child)

    return sum_total_size


f = open('input')
lines = f.readlines()

root = Directory(None)
current_directory = root
i = 0
while i < len(lines):
    cmd = lines[i].strip('\n')

    if cmd.startswith('$ cd'):
        path = cmd.split(' ')[2]

        if path == '/':
            current_directory = root
        elif path == '..':
            current_directory = current_directory.parent
        else:
            current_directory = current_directory.cd(path)

        i += 1
    elif cmd == '$ ls':
        i += 1

        while i < len(lines) and lines[i][0] != '$':
            item = lines[i].strip('\n').split(' ')

            if item[0] == 'dir':
                current_directory.mkdir(item[1])
            else:
                current_directory.touch(*item)

            i += 1

print(sum_total_sizes(root))
