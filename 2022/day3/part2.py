f = open('input')
lines = f.readlines()

a = ord('a')
z = ord('z')
A = ord('A')
Z = ord('Z')


def get_priority(item):
    if a <= ord(item) <= z:
        return ord(item) - a + 1
    else:
        return ord(item) - A + 27


priority_sum = 0

i = 0
while i < len(lines):
    rucksacks = [lines[i].strip('\n'), lines[i + 1].strip('\n'), lines[i + 2].strip('\n')]
    i += 3
    first_rucksack = set(get_priority(item) for item in rucksacks[0])
    second_rucksack = set(get_priority(item) for item in rucksacks[1])
    third_rucksack = set(get_priority(item) for item in rucksacks[2])

    priority_sum += sum(first_rucksack & second_rucksack & third_rucksack)

print(priority_sum)
