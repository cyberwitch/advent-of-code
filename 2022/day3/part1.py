f = open('input')
lines = f.readlines()

a = ord('a')
z = ord('z')
A = ord('A')
Z = ord('Z')


def get_priority(item):
    if a <= ord(item) <= z:
        return ord(item) - a + 1
    else:
        return ord(item) - A + 27


priority_sum = 0

for line in lines:
    rucksack = line.strip('\n')
    first_compartment = set(get_priority(item) for item in rucksack[0:(len(rucksack)//2)])
    second_compartment = set(get_priority(item) for item in rucksack[(len(rucksack)//2):])

    priority_sum += sum(first_compartment & second_compartment)

print(priority_sum)