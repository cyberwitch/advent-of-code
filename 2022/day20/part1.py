from dataclasses import dataclass


@dataclass
class Element:
    value: int
    next: 'Element' = None
    previous: 'Element' = None


@dataclass
class CircularLinkedList:
    elements: list[Element]

    def __init__(self, elements=None):
        self.elements = elements or []

    def append(self, e: int):
        if self.elements:
            ptr = Element(value=e, next=self.elements[0], previous=self.elements[-1])
            self.elements[0].previous = ptr
            self.elements[-1].next = ptr
            self.elements.append(ptr)
        else:
            e = Element(value=e)
            e.next = e
            e.previous = e
            self.elements = [e]

    def move(self, e: Element):
        if e != 0:
            e.previous.next = e.next
            e.next.previous = e.previous

        if e.value > 0:
            step = 1
        else:
            step = -1

        ptr = e.previous
        for i in range(0, e.value, step):
            if step == 1:
                ptr = ptr.next
            else:
                ptr = ptr.previous

        e.next = ptr.next
        e.previous = ptr
        ptr.next.previous = e
        ptr.next = e

    def get_groove_coordinate_sum(self):
        ptr = [e for e in self.elements if e.value == 0][0]
        for i in range(1000):
            ptr = ptr.next
        total = ptr.value
        for i in range(1000):
            ptr = ptr.next
        total += ptr.value
        for i in range(1000):
            ptr = ptr.next
        total += ptr.value
        return total


circular_linked_list = CircularLinkedList()

f = open('input')

for line in f.readlines():
    circular_linked_list.append(int(line))

for element in circular_linked_list.elements:
    circular_linked_list.move(element)

print(circular_linked_list.get_groove_coordinate_sum())
